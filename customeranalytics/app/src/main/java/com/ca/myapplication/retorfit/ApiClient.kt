package com.ca.myapplication.retorfit

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/*
* Retrofit client class
* Base URl
*
* */
class ApiClient {
    //Instance of retrofit builder
    private var mRetrofit: Retrofit? = null

    /*
    * creating retrofit builder to interact with api service
    * @param    NA
    * @return   Retrofit
    * */
    fun getClient(): Retrofit? {
        if (mRetrofit == null) {
            val oktHttpClient =
                OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)

            val gson = GsonBuilder()
                .setLenient()
                .create()
            mRetrofit = Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/") // Based on build variant automatically base url changed
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(oktHttpClient.build())
                .build()
        }
        return mRetrofit
    }
}