package com.ca.myapplication.retorfit

import com.ca.myapplication.retorfit.response.User
import retrofit2.Call
import retrofit2.http.*

/*
* API Interface for POST,GET,DELETE,PUT
* */
interface UserApi {
    // Users list api
    @Headers("Accept:application/json")
    @GET("users/")
    fun getUsers(): Call<ArrayList<User>>
}