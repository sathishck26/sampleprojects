package com.ca.myapplication.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.ca.myapplication.interfaces.ProfileActionClick
import com.ca.myapplication.R
import com.ca.myapplication.retorfit.response.User


class CustomAdapter(val userList: ArrayList<User>, val context: FragmentActivity, val profileActionClick: ProfileActionClick) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item_layout, parent, false)
        return ViewHolder(
            v,
            context,
            profileActionClick
        )
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View, private val context: Context, val profileActionClick: ProfileActionClick) :
        RecyclerView.ViewHolder(itemView) {
        fun bindItems(user: User) {
            val textViewName = itemView.findViewById(R.id.tvName) as TextView
            val textViewAddress = itemView.findViewById(R.id.tvAddress) as TextView
            val imageViewProfile = itemView.findViewById(R.id.ivProfile) as ImageView
            val imageViewGeo = itemView.findViewById(R.id.ivGeo) as ImageView
            val imageViewPhone = itemView.findViewById(R.id.ivMail) as ImageView
            imageViewGeo.setOnClickListener {
                val uri: String =
                    "http://maps.google.com/maps?q=loc:" + user.address.geo.lat + "," + user.address.geo.lng
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                context.startActivity(intent)
            }
            imageViewPhone.setOnClickListener {
                val to = arrayOf(user.email)
                val email = Intent(Intent.ACTION_SEND)
                email.putExtra(Intent.EXTRA_EMAIL, to)
                email.putExtra(Intent.EXTRA_SUBJECT, "")
                email.putExtra(Intent.EXTRA_TEXT, "")
                email.type = "message/rfc822"
                context.startActivity(Intent.createChooser(email, "Choose an Email client :"))
            }
            imageViewProfile.setOnClickListener {
                profileActionClick.onClicked(user)
            }
            textViewName.text = user.name
            textViewAddress.text =
                user.address.suite + "\n" + user.address.city + "\n" + user.address.street + "\n" + user.address.zipcode
        }
    }
}
