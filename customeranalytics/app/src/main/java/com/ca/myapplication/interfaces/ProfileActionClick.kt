package com.ca.myapplication.interfaces

import com.ca.myapplication.retorfit.response.User

interface ProfileActionClick {
    fun onClicked(user : User)
}