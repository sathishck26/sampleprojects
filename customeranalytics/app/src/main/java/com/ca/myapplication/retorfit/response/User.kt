package com.ca.myapplication.retorfit.response

data class User (
    var id : Int,
    var name : String,
    var username : String,
    var email : String,
    var phone : String,
    var address: Address
)