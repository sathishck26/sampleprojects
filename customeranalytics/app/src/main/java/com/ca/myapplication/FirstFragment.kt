package com.ca.myapplication

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ca.myapplication.adapter.CustomAdapter
import com.ca.myapplication.interfaces.ProfileActionClick
import com.ca.myapplication.retorfit.ApiClient
import com.ca.myapplication.retorfit.UserApi
import com.ca.myapplication.retorfit.response.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment(),
    ProfileActionClick {

    private lateinit var rvList: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvList = view.findViewById(R.id.recyclerView)
        getUsers()
    }

    /*
   * Retrofit Api call
   * Getting user list from the server.
   * @param   NA
   * @return  NA
   * */
    fun getUsers() {
        val retrofitApi: UserApi = ApiClient().getClient()!!.create(
            UserApi::class.java
        )
        val call: Call<ArrayList<User>> = retrofitApi.getUsers()
        call.enqueue(object : Callback<ArrayList<User>> {
            override fun onResponse(call: Call<ArrayList<User>>, response: Response<ArrayList<User>>) {
                try {
                    if (response.isSuccessful) {
                        Log.d(tag, "" + response.body())
                        setData(response.body())
                    } else {
                        sendErrorMsg("Un able to get data..")
                    }
                }
                catch (ex: Exception ) { sendErrorMsg(""+ex.message) }
            }

            override fun onFailure(call: Call<ArrayList<User>>, t: Throwable) {
                sendErrorMsg("" + t.message)
            }
        })
    }
    // Error Message sender
    private fun sendErrorMsg(message: String) {
        Log.e(tag, message)
    }

    private fun setData(users: ArrayList<User>?) {
        if(users != null && users.size>0) {
            rvList.layoutManager = LinearLayoutManager(this.activity, LinearLayout.VERTICAL, false)
            rvList.addItemDecoration(DividerItemDecoration(rvList.context, DividerItemDecoration.VERTICAL))
            //creating our adapter
            val adapter = CustomAdapter(
                users,
                activity!!,
                this
            )
            //now adding the adapter to recyclerview
            rvList.adapter = adapter
        }
    }

    override fun onClicked(user : User) {
        if (ContextCompat.checkSelfPermission(this.activity!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.activity!!, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        } else {
            takePic()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 0) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takePic()
            }
        }
    }

    private fun takePic() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 100) {
            //TODO
        }
    }
}
