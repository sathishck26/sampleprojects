package com.ca.myapplication.retorfit.response

data class Geo (
     var lat : String,
     var lng : String
)
